from bioservices import *
import xml.etree.ElementTree as ET
import StringIO


def __to_xml_key(namespace, element):
    """to search an element with a namespace """
    return '{'+namespace+"}"+element

#test if its work with the string
def __get_namespace(result):
    """ get the namespace from an opened xml result given by the API
    blastp of bioservices 
    @param result: the opened xml result given by the API of blastp 
    bioservices
    @type result: str
    @return: the namespace
    @rtype: str
    """
    #use of StringIO to give the behaviour of a file to the result 
    namespaces = dict([node for _, node in ET.iterparse(StringIO.StringIO(result), 
                                                        events=['start-ns'])])
    namespace = namespaces['']
    return namespace

def __get_all_hits(result, namespace):
    """ store the hit from a opened xml result given by the API blastp of
    bioservices
    @param result: the opened xml result given by the API of blastp 
    bioservices
    @type result: str
    @param namespace: the namespace (should be an url)
    @type namespace: str
    @return: the list of hits, hits containt the id, the name and the 
    score
    @rtype: list(dict())
    """
    hits = []
    root = ET.fromstring(result)
    for hit in root.iter(__to_xml_key(namespace, 'hit')):
        dhit = eval(str(hit.attrib))
        scores = []
        for score in hit.iter(__to_xml_key(namespace, 'score')):
            scores.append(float(score.text))
        dhit["score"] = max(scores)
        hits.append(dhit)
    return hits

def __hits_sequences(sequences, database, email):
    """ get all hits (hits of each sequences)
    @param sequences: a list of sequence
    @type sequences: list(str)
    @param database: the name of the database to search on
    @type database: str
    @param email: the email of the user
    @type email: str
    @return: a list of hits
    @rtype: list(dict())
    """
    hits = []
    for sequence in sequences:
        s = NCBIblast(verbose=False)
        jobid = s.run(program="blastp", sequence=sequence, stype="protein",
                    database=database, email=email)
        xml = str(s.getResult(jobid, 'xml'))
        namespace = __get_namespace(xml)
        hits += __get_all_hits(xml, namespace)
    return hits
    
def __process_hits(hits):
    """ combine hits when they have the same name (addition of the score)
    @param hits: a list of hits
    @type hits: list(dict())
    @return: the hits why there id as key
    @rtype: dict(str:dict())
    """
    res_hits = {}
    for hit in hits:
        tmphit = res_hits.get(hit["id"], {})
        if tmphit != {}:
            tmphit["score"] = tmphit["score"] + hit["score"]
        else:
            tmphit["description"] = hit["description"]
            tmphit["database"] = hit["database"] 
            tmphit["score"] = hit["score"]
        res_hits[hit["id"]] = tmphit
    return res_hits

def all_hits(sequences, database, email):
    """get all hits (hits of each sequences)
    @param sequences: a list of sequence
    @type sequences: list(str)
    @param database: the name of the database to search on.
    can be "uniprotkb" need to now other name
    @type database: str
    @param email: the email of the user
    @type email: str
    @return: a list of hits
    @rtype: list(dict())
    """
    hits = __hits_sequences(sequences, 
                            database=database, email=email)
    return __process_hits(hits)


#~ print all_hits(["MPDPAAHLPFFYGSIS", "KYCIPEGTKFDTL"], "uniprotkb", "nothing@hotmail.fr")


